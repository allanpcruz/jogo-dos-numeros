let numeroCorreto = criarNumeroCorreto();
let tempoRestante = criarTempoRestante();
let tentativas = criarTentativas()
let jogoAcabou = false;
let jogoComecou = false;

const body = document.body;
const input = document.querySelector("#palpite");
const buttons = document.querySelectorAll("button");
const temaToggle = document.querySelector('.tema-toggle');

atualizarTempoRestante();
document.querySelector('#adivinhar').addEventListener('click', adivinharNumero);
document.querySelector('#reiniciar').addEventListener('click', reiniciarJogo);
document.querySelector('.tema-toggle').addEventListener('click', toggleDarkMode);
document.querySelector('.bars').addEventListener('click', toggleMenu);

document.querySelector('#tentativas').textContent = `Tentativas restantes: ${tentativas}`;
document.querySelector('#tempo').textContent = `Tempo restante: ${tempoRestante}`;

function adivinharNumero() {
    if (!jogoComecou) {
        jogoComecou = true;
        atualizarTempoRestante();
    }

    var numeroDigitado = document.querySelector('#palpite').value;

    if (numeroDigitado == numeroCorreto) {
        iniciarSom()
        jogoAcabou = true;
        document.querySelector('#mensagem').textContent = 'Parabéns!!! Você acertou!!!';
        document.querySelector('#adivinhar').disabled = true;
        document.querySelector('#reiniciar').classList.remove('hidden');
    } else if (numeroDigitado < numeroCorreto) {
        document.querySelector('#mensagem').textContent = 'Tente um numero maior';
    } else {
        document.querySelector('#mensagem').textContent = 'Tente um numero menor';
    }

    tentativas--;

    document.querySelector('#tentativas').textContent = `Tentativas restantes: ${tentativas}`;

    if (tentativas == 0 && !jogoAcabou) {
        jogoAcabou = true;
        iniciarSom()
        document.querySelector('#mensagem').textContent = `Fim de jogo. O número correto era ${numeroCorreto}`;
        document.querySelector('#adivinhar').disabled = true;
        document.querySelector('#reiniciar').classList.remove('hidden');
    }

    document.querySelector('#palpite').focus();
    document.querySelector('#palpite').select();

}

function criarNumeroCorreto() {
    var numeroCriado = Math.floor(Math.random() * 100) + 1;
    console.log("🚀 ~ file: script.js:54 ~ criarNumeroCorreto ~ numeroCriado:", numeroCriado);
    return numeroCriado;
}

function criarTempoRestante() {
    return 15;
}

function criarTentativas() {
    return 10;
}

function reiniciarJogo() {
    jogoAcabou = false;
    jogoComecou = false;
    numeroCorreto = criarNumeroCorreto();
    tempoRestante = criarTempoRestante();
    tentativas = criarTentativas();
    document.querySelector('#mensagem').textContent = 'Digite um número';
    document.querySelector('#adivinhar').disabled = false;
    document.querySelector('#reiniciar').classList.add('hidden');
    document.querySelector('#palpite').value = '';
    document.querySelector('#tentativas').textContent = `Tentativas restantes: ${tentativas}`;
    document.querySelector('#tempo').textContent = `Tempo restante: ${tempoRestante}`;
}

function atualizarTempoRestante() {
    if (jogoComecou && !jogoAcabou) {
        if (tempoRestante > 0) {
            tempoRestante--;
            document.querySelector('#tempo').textContent = `Tempo restante: ${tempoRestante}`;
            setTimeout(atualizarTempoRestante, 1000);
        } else {
            jogoAcabou = true;
            iniciarSom()
            document.querySelector('#mensagem').textContent = `Fim de jogo. O número correto era ${numeroCorreto}`;
            document.querySelector('#adivinhar').disabled = true;
            document.querySelector('#reiniciar').classList.remove('hidden');
        }
    }
}

function toggleDarkMode() {
    body.classList.toggle("dark-mode");
    input.classList.toggle("dark-mode");
    buttons.forEach(button => button.classList.toggle("dark-mode"));

    if (body.classList.contains("dark-mode")) {
        temaToggle.innerHTML = '<i class="fas fa-sun"></i>';
        localStorage.setItem('modo', 'dark-mode');
    } else {
        temaToggle.innerHTML = '<i class="fas fa-moon"></i>';
        localStorage.setItem('modo', '');
    }
}

function definirIconeModo() {
    if (body.classList.contains("dark-mode")) {
        temaToggle.innerHTML = '<i class="fas fa-sun"></i>';
    } else {
        temaToggle.innerHTML = '<i class="fas fa-moon"></i>';
    }
}

function toggleMenu() {
    const nivelJogo = 0 || localStorage.getItem('nivel');
    const menuContainer = document.querySelector('#menu-container');
    menuContainer.style.display = (menuContainer.style.display === 'none' || menuContainer.style.display === '') ? 'flex' : 'none';

    const menuItens = document.querySelectorAll('.opcoes')
    menuItens.forEach(item => {
        if (item.textContent.includes('Sem Restrições') && nivelJogo == 1) {
            item.classList.add('selected');
        } else if (item.textContent.includes('Jogo com Tentativas') && nivelJogo == 2) {
            item.classList.add('selected');
        } else if (item.textContent.includes('Jogo com Tempo') && nivelJogo == 3) {
            item.classList.add('selected');
        } else if (item.textContent.includes('Jogo com Ambos') && nivelJogo == 4) {
            item.classList.add('selected');
        }
    });

}

function fecharMenu() {
    const menuContainer = document.querySelector('#menu-container');
    menuContainer.style.display = 'none';
}

window.addEventListener('load', () => {
    const modoSalvo = localStorage.getItem('modo');

    if (modoSalvo === 'dark-mode') {
        body.classList.add('dark-mode');
        buttons.forEach(button => button.classList.toggle("dark-mode"));
        input.classList.toggle("dark-mode");
    }

    definirIconeModo();

    const menuItens = document.querySelectorAll('.opcoes')
    let nivelJogo = 0
    menuItens.forEach(item => {
        item.addEventListener('click', function () {
            menuItens.forEach(item => {
                item.classList.remove('selected');
            });

            this.classList.add('selected');

            if (this.textContent.includes('Sem Restrições')) {
                nivelJogo = 1;
            } else if (this.textContent.includes('Jogo com Tentativas')) {
                nivelJogo = 2;
            } else if (this.textContent.includes('Jogo com Tempo')) {
                nivelJogo = 3;
            } else if (this.textContent.includes('Jogo com Ambos')) {
                nivelJogo = 4;
            }
            localStorage.setItem('nivel', nivelJogo);
        });
    });
});

function iniciarSom() {
    context = new AudioContext();
    oscillator = context.createOscillator();
    contextGain = context.createGain();

    oscillator.connect(contextGain);
    contextGain.connect(context.destination);
    oscillator.start(0);

    stop()
}

function stop() {
    contextGain.gain.exponentialRampToValueAtTime(
        0.00001, context.currentTime + 6
    )
}
