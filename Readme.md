# Jogo dos Números

Um jogo envolvente de adivinhação de números criado com HTML, CSS e JavaScript.

## Recursos

* Jogo com Tempo:
  * O jogador tem 15 segundos para adivinhar o número correto.
* Jogo com Tentativas:
	* O jogador dispõe de 10 tentativas para adivinhar o número correto.
* Jogo com Ambos:
	* O jogador possui 15 segundos e 10 tentativas para adivinhar o número correto.
* Jogo Sem Restrições:
	* O jogador pode jogar sem restrições. O jogo termina quando o número correto é adivinhado.
* Estatísticas:
    * Acompanhe a quantidade de partidas jogadas.
  * Veja quantas partidas foram ganhas.
  * Explore os melhores placares tanto em jogos com tempo, tentativas ou ambos.

## Objetivo do Projeto
Este jogo foi criado com o intuito de ensinar lógica de programação para crianças de forma lúdica. Durante o desenvolvimento, foram abordados diversos temas:

### HTML
* Estrutura Básica: Definindo a estrutura básica do documento HTML.
* Metadados: Especificando metadados como charset e viewport.
* Título e Estilos: Definindo o título da página e vinculando folhas de estilo.
* Corpo da Página: Estruturando elementos do jogo, como títulos, mensagens, entrada e botões.
* Menu Responsivo: Implementando um menu com opções interativas.
  
### CSS
* Estrutura do CSS: Organizando o código CSS para facilitar a manutenção e leitura.
* Seletores Básicos: Aplicando estilos a elementos HTML, como texto, botões e entrada.
* Seleção de Elementos Específicos: * Estilizando elementos específicos, como mensagens e botões de controle.
* Alternância entre Tema Claro e Escuro: Implementando um interruptor de tema para aprimorar a experiência do usuário.
* Menu Responsivo: Estilizando um menu que se adapta a diferentes dispositivos.
* Media Queries: Utilizando media queries para criar uma experiência responsiva em telas menores.
### JavaScript
* Criação de Variáveis e Constantes: Declarando variáveis e constantes para armazenar dados do jogo.
* Seletores do DOM: Utilizando seletores para interagir com elementos HTML no JavaScript.
* Event Listeners: Adicionando manipuladores de eventos para tornar o jogo interativo.
* Condições e Controles de Fluxo: Implementando lógica condicional para guiar o fluxo do jogo.
* Laços de Repetição: Utilizando loops para repetir ações, como atualizar o tempo restante.
* Manipulação de Elementos do DOM: Modificando dinamicamente o conteúdo e o estilo dos elementos na página.
* Funções: Criando funções para organizar o código e reutilizar blocos de lógica.
* Armazenamento Local: Utilizando armazenamento local para manter as preferências do usuário.
* Manipulação de Classes CSS: Alterando classes para aplicar estilos diferentes ao interruptor de tema e ao menu.
* Recursividade: Implementando uma função recursiva para atualizar o tempo restante no jogo.
* Contexto de Áudio: Criando um contexto de áudio para reproduzir sons quando o jogador acerta.


Este jogo é uma ferramenta educacional envolvente que ensina conceitos de programação de forma prática e interativa. Divirta-se enquanto aprende!




